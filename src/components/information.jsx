import React from 'react';
import { Page, Card, Navbar, Icon, Box, Text, Tabs, Link, Tab, Tabbar, useStore, Checkbox, Row, Col, Button, Fab, Avatar } from "zmp-framework/react";
import '../css/information.css'



const Information = () => {
    return (
    <Page name="information">
      <Navbar backLink="Back" className="navbar">
        <p>Đặt Trước </p>
      </Navbar>
    
      <Card className="information-user">
        <h3>THÔNG TIN LIÊN LẠC</h3>
        <p>Họ và tên: tamy </p>
        <p>Số điện thoại: 0974675965 </p>
        <p>Địa chỉ: 89 song hành, Phường An Phú, Quận 2, Thành phố Hồ Chí Minh </p>
      </Card>

      <Card className="information-address">
        <div className ="information-address-text">
          <h3>ĐỊA CHỈ NHẬN HÀNG</h3>
          <p>Đơn hàng của bạn sẽ được trưởng nhóm mua xác nhận. Sản phẩm sẽ được chuyển về địa chỉ của trưởng nhóm</p>
          <p>Vui lòng liên hệ với trưởng nhóm mua của bạn để nhận hàng</p>
        </div>
        <Box className = "information-address-item">
          <div className="address-user">
            <Row>
          <Col>
            <Box className="name-user">
              <li><b>Jennie Thanh</b></li>
              <li><b>0356029928</b></li>
              <p>Địa chỉ: 92 Nguyen Huu Canh, Phuong 22, Quận Bình Thạnh, TP.Hồ Chí Minh</p>
            </Box>
            <Box>
              <b>Sản phẩm</b>
            </Box>
            <Box>
              <Row>
          <Col width="33">
            <Box>
              
            </Box>
          </Col>
          <Col width="66">
            <Box>
              Flex 5_2
            </Box>
          </Col>
          
        </Row>
            </Box>
          </Col>
        </Row>
              
          </div>
          <div className="information-item"></div>
        </Box>
      </Card>

      <Card className="payment">
          <h3>PHƯƠNG THỨC THANH TOÁN</h3>
          <Checkbox name="hobbies" label="Thẻ ATM nội địa" checked="checkbox" />
          <Checkbox name="hobbies" label="Internet banking"  />
          <Checkbox name="hobbies" label="Thẻ quốc tế Visa, Master, JCB"  />
      </Card>

      <Card className="Purchase">
        <Row>
          <Col>
            <p>Tổng cộng</p>
            <p className="total-items">₫60.000</p>
          </Col>
          <Col>
            <Box flex flexDirection="col" nowrap>
              <Box>
                <Button className="filter-button" typeName="primary">
                  <p>Đặt mua (1)</p>
                </Button>
              </Box>
            </Box>
          </Col>
        </Row>
      </Card>
    </Page>
    
  );
}
export default Information;